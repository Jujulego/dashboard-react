// Importations
import Track from "../schema/track.schema";
import utils from "../utils";

// Utils
function msg500(verb, id = null) {
    if (id != null) {
        return `Internal error while ${verb} track ${id}`
    } else {
        return `Internal error while ${verb} track(s)`
    }
}

function msg404(id) {
    return `Track ${id} not found`
}

// Méthodes
// - accès
export function all(res, cb) {
    // Query
    Track.find()
        .exec(utils.check_error_500(res, msg500("getting"), cb));
}

export function get(id, res, cb) {
    // Query
    Track.findById(id)
        .exec(utils.check_error_404(res, msg404(id), msg500("getting", id), cb));
}

export function search(filter, res, cb) {
    // Query
    Track.find(filter)
        .exec(utils.check_error_500(res, msg500("getting"), cb))
}

export function aggregate(pipeline, res, cb) {
    // Query
    Track.aggregate(pipeline)
        .exec(utils.check_error_500(res, msg500("computing on"), cb))
}

export function count(res, cb) {
    // Query
    Track.estimatedDocumentCount()
        .exec(utils.check_error_500(res, msg500("computing on"), cb));
}

// - modification
export function insert({ title, duration, listenings, likes }, res, cb) {
    // Query
    const track = new Track({ title, duration, listenings, likes });
    track.save(utils.check_error_500(res, msg500("creating"), cb));
}

export function save(artist, res, cb) {
    artist.save(utils.check_error_500(res, msg500("updating"), cb));
}

export function remove(id, res, cb) {
    // Query
    Track.findByIdAndDelete(id)
        .exec(utils.check_error_500(res, msg500("deleting", id), cb));
}

export function update(id, { title, duration, listenings, likes }, res, cb) {
    // Query track
    get(id, res, (track) => {
        // Update
        if (title)      track.title      = title;
        if (duration)   track.duration   = duration;
        if (listenings) track.listenings = listenings;
        if (likes)      track.likes      = likes;

        save(track, res, cb);
    });
}

export default { all, get, search, aggregate, count, insert, save, remove, update };