// Importations
import Album from "../schema/album.schema";
import utils from "../utils";

// Utils
function msg500(verb, id = null) {
    if (id != null) {
        return `Internal error while ${verb} album ${id}`
    } else {
        return `Internal error while ${verb} album(s)`
    }
}

function msg404(id) {
    return `Album ${id} not found`
}

// Méthodes
// - accès
export function all(res, cb) {
    // Query
    Album.find()
        .exec(utils.check_error_500(res, msg500("getting"), cb));
}

export function get(id, res, cb) {
    // Query
    Album.findById(id)
        .exec(utils.check_error_404(res, msg404(id), msg500("getting", id), cb));
}

export function search(filter, res, cb) {
    // Query
    Album.find(filter)
        .exec(utils.check_error_500(res, msg500("getting"), cb))
}

export function aggregate(pipeline, res, cb) {
    // Query
    Album.aggregate(pipeline)
        .exec(utils.check_error_500(res, msg500("computing on"), cb))
}

export function count(res, cb) {
    // Query
    Album.estimatedDocumentCount()
        .exec(utils.check_error_500(res, msg500("computing on"), cb));
}

// - modification
export function insert({ title, genre, cover, release }, res, cb) {
    // Check values
    if (isNaN(Date.parse(release))) {
        return utils.badrequest_error(res, 'release is not a valid date');
    } else {
        release = new Date(release);
    }

    // Query
    const album = new Album({ title, genre, cover, release });
    album.save(utils.check_error_500(res, msg500("creating"), cb));
}

export function save(artist, res, cb) {
    artist.save(utils.check_error_500(res, msg500("updating"), cb));
}

export function remove(id, res, cb) {
    // Query
    Album.findByIdAndDelete(id)
        .exec(utils.check_error_500(res, msg500("deleting", id), cb));
}

export function update(id, { title, genre, cover, release }, res, cb) {
    // Check values
    if (isNaN(Date.parse(release))) {
        return utils.badrequest_error(res, 'release is not a valid date');
    } else {
        release = new Date(release);
    }

    // Query album
    get(id, res, (album) => {
        // Update
        if (title)   album.title   = title;
        if (release) album.release = release;
        if (genre)   album.genre   = genre;
        if (cover)   album.cover   = cover;

        save(album, res, cb);
    });
}

export default { all, get, search, aggregate, count, insert, save, remove, update };