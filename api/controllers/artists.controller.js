// Importations
import Artist from "../schema/artist.schema";
import utils from "../utils";

// Utils
function msg500(verb, id = null) {
    if (id != null) {
        return `Internal error while ${verb} artist ${id}`
    } else {
        return `Internal error while ${verb} artist(s)`
    }
}

function msg404(id) {
    return `Artist ${id} not found`
}

// Méthodes
// - accès
export function all(res, cb) {
    // Query
    Artist.find()
        .exec(utils.check_error_500(res, msg500("getting"), cb));
}

export function get(id, res, cb) {
    // Query
    Artist.findById(id)
        .exec(utils.check_error_404(res, msg404(id), msg500("getting", id), cb));
}

export function search(filter, res, cb) {
    // Query
    Artist.find(filter)
        .exec(utils.check_error_500(res, msg500("getting"), cb))
}

export function textSearch(text, res, cb) {
    // Query
    Artist.find({ $text: { $search: text } }, { score: { $meta: "textScore" } })
        .sort({ score: { $meta: "textScore" }})
        .exec(utils.check_error_500(res, msg500("getting"), cb))
}

export function aggregate(pipeline, res, cb) {
    // Query
    Artist.aggregate(pipeline)
        .exec(utils.check_error_500(res, msg500("computing on"), cb))
}

export function count(res, cb) {
    // Query
    Artist.estimatedDocumentCount()
        .exec(utils.check_error_500(res, msg500("computing on"), cb));
}

// - modification
export function insert({ name, birth, first_name, followers }, res, cb) {
    // Check values
    if (isNaN(Date.parse(birth))) {
        return utils.badrequest_error(res, 'birth is not a valid date');
    } else {
        birth = new Date(birth);
    }

    // Query
    const artist = new Artist({ name, birth, first_name, followers });
    artist.save(utils.check_error_500(res, msg500("creating"), cb));
}

export function save(artist, res, cb) {
    artist.save(utils.check_error_500(res, msg500("updating"), cb));
}

export function remove(id, res, cb) {
    // Query
    Artist.findByIdAndDelete(id)
        .exec(utils.check_error_500(res, msg500("deleting", id), cb));
}

export function update(id, { name, birth, first_name, followers }, res, cb) {
    // Check values
    if (isNaN(Date.parse(birth))) {
        return utils.badrequest_error(res, 'birth is not a valid date');
    } else {
        birth = new Date(birth);
    }

    // Query artist
    get(id, res, (artist) => {
        // Update
        if (name)       artist.name = name;
        if (birth)      artist.birth = birth;
        if (first_name) artist.first_name = first_name;
        if (followers)  artist.followers  = followers;

        save(artist, res, cb);
    });
}

export default { all, get, search, textSearch, aggregate, count, insert, save, remove, update };