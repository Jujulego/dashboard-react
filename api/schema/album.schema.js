// Importations
import mongoose, { Schema } from "mongoose";
const { ObjectId } = Schema.Types;

// Schema
const schema = Schema({
    title:   { type: String, required: true },
    release: { type: Date,   required: true },
    genre:   { type: String, required: true },
    cover:   { type: String },
    tracks:  [ ObjectId ]
});

// Model
const Album = mongoose.model('Album', schema);

export default Album;