// Importations
import mongoose, { Schema } from "mongoose";

const { ObjectId } = Schema.Types;

// Schema
const schema = Schema({
    name:       { type: String, required: true },
    birth:      { type: Date,   required: true },
    first_name: { type: String },
    followers:  { type: Number, default: 0, min: 0 },
    albums:     [ ObjectId ]
});

schema.index(
    { name: "text", first_name: "text" },
    { weights: { name: 2, first_name: 1 }}
);

// Model
const Artist = mongoose.model('Artist', schema);

export default Artist;