// Importations
import mongoose, { Schema } from "mongoose";
const { ObjectId } = Schema.Types;

// Schema
const schema = Schema({
    title:      { type: String, required: true },
    duration:   { type: Number, required: true },
    listenings: { type: Number, default: 0, min: 0 },
    likes:      { type: Number, default: 0, min: 0 },
    featuring:  [ ObjectId ]
});

// Model
const Track = mongoose.model('Track', schema);

export default Track;