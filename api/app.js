// Importations
import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import morgan from "morgan";

import config from "./config/database.config";

import artistsRouter from "./routes/artists.router";
import albumsRouter  from "./routes/albums.router";
import tracksRouter  from "./routes/tracks.router";

// Connect to database
mongoose.connect(config.url, { useNewUrlParser: true })
    .then(function() {
        console.log("Connected to MongoDB");
    });

// Initialisation
const PORT = process.env.PORT || 8000;
const app = express();

// Middlewares
app.use(morgan("dev"));

app.use(cors());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Routes
app.use("/api/artist/", artistsRouter);
app.use("/api/album/",  albumsRouter);
app.use("/api/track/",  tracksRouter);

// Lancement
app.listen(PORT, function() {
    console.log(`Listening at http://localhost:${PORT}/`);
});