// Importations
import { Router } from "express";

import albums from "../controllers/albums.controller";
import tracks from "../controllers/tracks.controller";
import utils from "../utils";

// Router
const router = Router();

// Routes
router.get("/all", function(req, res) {
    albums.all(res, (albums) => res.json(albums));
});

router.put("/", utils.has_params(["title", "release", "genre"], function(req, res) {
    albums.insert(req.body, res, (album) => res.json(album));
}));

router.route("/:id")
    .get(function(req, res) {
        albums.get(req.params.id, res, (album) => res.json(album));
    })
    .post(function(req, res) {
        albums.update(req.params.id, req.body, res, (album) => res.json(album));
    })
    .delete(function(req, res) {
        albums.remove(req.params.id, res, () => res.json({ msg: "album deleted" }));
    });

router.route('/:id/tracks')
    .get(function(req, res) {
        albums.get(req.params.id, res,
            function(album) {
                // Get tracks
                tracks.search({ _id: { $in: album.tracks }}, res, (tracks) => res.json(tracks));
            }
        );
    })
    .put(function(req, res) {
        // Parameters
        const { id } = req.params;
        const { track } = req.body;

        // Check track
        tracks.get(track, res, (track) => {
            // Get album
            albums.get(id, res, (album) => {
                // Ajout de l'album
                album.tracks.push(track._id);

                // Sauvegarde
                albums.save(album, res, (album) => res.json(album));
            });
        });
    });

router.delete('/:id/tracks/:track', function(req, res) {
    // Parameters
    const { id, track } = req.params;

    // Get album
    albums.get(id, res, (album) => {
        // Suppression de l'album
        const i = album.tracks.indexOf(track);
        album.tracks.splice(i, 1);

        // Sauvegarde
        albums.save(album, res, (album) => res.json(album));
    });
});

// Stats
router.get('/stats/count', function(req, res) {
    albums.count(res, (result) => res.json(result));
});

router.get('/stats/genres', function(req, res) {
    // Requête
    albums.aggregate([
        { $group: {
            _id: "$genre",
            nb: { $sum: 1 }
        }}
    ], res, (result) => res.json(result));
});

router.get('/stats/releases', function(req, res) {
    // Date
    const now = new Date();
    const lastYear = new Date(now.getFullYear()-1, now.getMonth(), 1);

    // utils
    function eq(id1, id2) {
        return id1.year === id2.year && id1.month === id2.month;
    }

    function inf(id1, id2) {
        if (id1.year === id2.year) {
            return id1.month < id2.month
        }

        return id1.year < id2.year;
    }

    // Aggregate
    albums.aggregate([
        { $match: {
            release: { $gt: lastYear }
        }},
        { $group: {
            _id: {
                month: { $month: "$release" },
                year: { $year: "$release" }
            },
            nb: { $sum: 1 }
        }},
        { $sort: {
            "_id.year": 1,
            "_id.month": 1,
        }}
    ], res, function (result) {
        const dep = { year: lastYear.getFullYear(), month: lastYear.getMonth()+1 };
        const r = [];
        let j = 0;

        for (let i = 0; i < 13; ++i) {
            // Generation mois suivant
            const id = { year: dep.year, month: dep.month + i };
            while (id.month > 12) {
                id.month = id.month - 12;
                id.year++;
            }

            // Existe ?
            while (j < result.length && inf(result[j]._id, id)) {
                ++j;
            }

            if (j < result.length && eq(result[j]._id, id)) {
                r.push(result[j])
            } else {
                r.push({ _id: id, nb: 0 });
            }
        }

        res.json(r);
    });
});

export default router;