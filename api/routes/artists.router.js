// Importations
import { Router } from "express";

import artists from "../controllers/artists.controller";
import albums from "../controllers/albums.controller";
import utils from "../utils";

// Router
const router = Router();

// Routes
router.get("/all", function(req, res) {
    artists.all(res, (artists) => res.json(artists));
});

router.get("/search", utils.has_params(["q"], function(req, res) {
    artists.textSearch(req.query.q, res, (artists) => res.json(artists))
}));

router.put("/", utils.has_params(["name", "birth"], function(req, res) {
    artists.insert(req.body, res, (artist) => res.json(artist));
}));

router.route("/:id")
    .get(function(req, res) {
        artists.get(req.params.id, res, (artist) => res.json(artist));
    })
    .post(function(req, res) {
        artists.update(req.params.id, req.body, res, (artist) => res.json(artist));
    })
    .delete(function(req, res) {
        artists.remove(req.params.id, res, () => res.json({ msg: "Artist deleted" }));
    });

router.route('/:id/albums')
    .get(function(req, res) {
        artists.get(req.params.id, res, (artist) => {
            // Get albums
            albums.search({ _id: { $in: artist.albums }}, res, (albums) => res.json(albums));
        });
    })
    .put(function(req, res) {
        // Parameters
        const { id } = req.params;
        const { album } = req.body;

        // Check if album exists
        albums.get(album, res, (album) => {
            // Get artist
            artists.get(id, res, (artist) => {
                // Ajout de l'album
                artist.albums.push(album._id);

                // Sauvegarde
                artists.save(artist, res, (artist) => res.json(artist));
            });
        });
    });

router.delete('/:id/albums/:album', function(req, res) {
        // Parameters
        const { id, album } = req.params;

        // Get artist
        artists.get(id, res, (artist) => {
            // Suppression de l'album
            const i = artist.albums.indexOf(album);
            artist.albums.splice(i, 1);

            // Sauvegarde
            artists.save(artist, res, (artist) => res.json(artist));
        });
    });

// Stats
router.get('/stats/count', function(req, res) {
    // Requête
    artists.count(res, (result) => res.json(result));
});

router.get('/stats/stars', function(req, res) {
    // Params
    const { nb } = req.query;

    // Query
    artists.aggregate(
        [
            { $sort: {
                followers: -1
            }},
            { $limit: +nb || 5 },
        ], res, (result) => res.json(result)
    );
});

router.get('/stats/stars/stats', function(req, res) {
    // Params
    const { nb } = req.query;

    // Query
    artists.aggregate(
        [
            { $sort: {
                followers: -1
            }},
            { $limit: +nb || 5 },
            { $lookup: {
                from: 'albums',
                localField: 'albums',
                foreignField: '_id',
                as: 'albums'
            }},
            { $unwind: {
                path: "$albums",
                preserveNullAndEmptyArrays: true
            }},
            { $lookup: {
                from: 'tracks',
                localField: '_id',
                foreignField: 'featuring',
                as: 'tracks'
            }},
            { $unwind: {
                path: "$tracks",
                preserveNullAndEmptyArrays: true
            }},
            { $group: {
                _id: { _id: "$_id", album: "$albums._id" },
                name:       { $first: "$name" },
                first_name: { $first: "$first_name" },
                followers:  { $first: "$followers" },

                listenings: { $sum: "$tracks.listenings" },
                likes:      { $sum: "$tracks.likes" },
                nb_tracks:  { $sum: 1 },
            }},
            { $group: {
                _id: "$_id._id",
                name:       { $first: "$name" },
                first_name: { $first: "$first_name" },
                followers:  { $first: "$followers" },

                listenings: { $sum: "$listenings" },
                likes:      { $sum: "$likes" },
                nb_tracks:  { $sum: "$nb_tracks" },
                nb_albums:  { $sum: 1 },
            }},
            { $addFields: {
                full_name: { $concat: ["$first_name", " ", "$name"] }
            }},
            { $sort: {
                followers: -1
            }}
        ], res, (result) => res.json(result)
    );
});

export default router;