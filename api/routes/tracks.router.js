// Importations
import { Router } from "express";

import artists from "../controllers/artists.controller";
import tracks from "../controllers/tracks.controller";
import utils from "../utils";

// Router
const router = Router();

// Routes
router.get("/all", function(req, res) {
    tracks.all(res, (tracks) => res.json(tracks));
});

router.put("/", utils.has_params(["title", "duration"], function(req, res) {
    tracks.insert(req.body, res, (track) => res.json(track));
}));

router.route("/:id")
    .get(function(req, res) {
        tracks.get(req.params.id, res, (track) => res.json(track));
    })
    .post(function(req, res) {
        tracks.update(req.params.id, req.data, res, (track) => res.json(track));
    })
    .delete(function(req, res) {
        tracks.remove(req.params.id, res, () => res.json({ msg: "track deleted" }));
    });

router.route('/:id/featuring')
    .get(function(req, res) {
        // Get track
        tracks.get(req.params.id, res, (track) => {
            // Get artists
            artists.search({ _id: { $in: track.featuring }}, res, (artists) => res.json(artists));
        });
    })
    .put(function(req, res) {
        // Parameters
        const { id } = req.params;
        const { artist } = req.body;

        // Check if artist exists
        artists.get(artist, res, (artist) => {
            // Get track
            tracks.get(id, res, (track) => {
                // Ajout de l'artist
                track.featuring.push(artist._id);

                // Sauvegarde
                tracks.save(track, res, (track) => res.json(track));
            });
        });
    });

router.delete('/:id/featuring/:artist', function(req, res) {
    // Parameters
    const { id, artist } = req.params;

    // Get track
    tracks.get(id, res, (track) => {
            // Suppression de l'artist
            const i = track.featuring.indexOf(artist);
            track.featuring.splice(i, 1);

            // Sauvegarde
            tracks.save(track, res, (track) => res.json(track));
        }
    );
});

export default router;