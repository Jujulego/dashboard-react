// Imports
import React from 'react';

import { storiesOf } from '@storybook/react';
import { withInfo } from "@storybook/addon-info";
import { withKnobs, array, number, object, text } from "@storybook/addon-knobs";

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

import BarChartCard from "../Components/Cards/BarChartCard";
import DarkTheme from "../Components/DarkTheme";
import LineChartCard from "../Components/Cards/LineChartCard";
import ListCard from "../Components/Cards/ListCard";
import NumberCard from "../Components/Cards/NumberCard";
import PieCard from "../Components/Cards/PieCard";
import RankingCard from "../Components/Cards/RankingCard";

// FontAwesome
library.add(fas);

// Data
const linedata = [
    {
        "name": "Page A",
        "uv": 4000,
        "pv": 2400,
        "amt": 2400
    },
    {
        "name": "Page B",
        "uv": 3000,
        "pv": 1398,
        "amt": 2210
    },
    {
        "name": "Page C",
        "uv": 2000,
        "pv": 9800,
        "amt": 2290
    },
    {
        "name": "Page D",
        "uv": 2780,
        "pv": 3908,
        "amt": 2000
    },
    {
        "name": "Page E",
        "uv": 1890,
        "pv": 4800,
        "amt": 2181
    },
    {
        "name": "Page F",
        "uv": 2390,
        "pv": 3800,
        "amt": 2500
    },
    {
        "name": "Page G",
        "uv": 3490,
        "pv": 4300,
        "amt": 2100
    }
];
const piedata = [
    {
        "name": "Group A",
        "value": 400
    },
    {
        "name": "Group B",
        "value": 300
    },
    {
        "name": "Group C",
        "value": 300
    },
    {
        "name": "Group D",
        "value": 200
    },
    {
        "name": "Group E",
        "value": 278
    },
    {
        "name": "Group F",
        "value": 189
    }
];

// Stories
storiesOf("Basics|Cards", module)
    .addDecorator(withInfo)
    .addDecorator(withKnobs)
    .add("ListCard", () => {
        // Props
        let titre = text("title", "Test");
        let data  = array("items", ["Test1", "Test2", "Test3"]);

        // Rendering
        return <ListCard title={ titre } items={ data } />;
    })
    .add("NumberCard", () => {
        // Props
        let titre  = text("title", "Test");
        let icon   = text("icon");
        let valeur = number("value", 5);
        let unit   = text("unit");

        // Rendering
        if (icon === "") { icon = undefined; }
        if (unit === "") { unit = undefined; }

        return <NumberCard title={ titre } icon={ icon } value={ valeur } unit={ unit } />;
    })
    .add("RankingCard", () => {
        // Props
        let titre = text("title", "Test");
        let data  = array("items", ["Test1", "Test2", "Test3"]);

        // Rendering
        return <RankingCard title={ titre } items={ data } />;
    });

storiesOf("Basics|Cards.Charts", module)
    .addDecorator(withInfo)
    .addDecorator(withKnobs)
    .add("BarChart",  () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = array("values", ["uv"]);
        let colors = object("colors");
        let data   = object("data", linedata);

        // Rendering
        return <BarChartCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } />;
    })
    .add("LineChart", () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = array("values", ["uv"]);
        let colors = object("colors");
        let data   = object("data", linedata);

        // Rendering
        return <LineChartCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } />;
    })
    .add("PieChart",  () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = text("values", "value");
        let colors = object("colors");
        let data   = object("data", piedata);

        // Rendering
        return <PieCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } />;
    });

storiesOf("DarkTheme|Cards", module)
    .addDecorator(story => <div style={{ padding: 16 }}>{ story() }</div>)
    .addDecorator(withKnobs)
    .add("ListCard", () => {
        // Props
        let titre = text("title", "Test");
        let data  = array("items", ["Test1", "Test2", "Test3"]);

        // Rendering
        return <DarkTheme><ListCard title={ titre } items={ data } /></DarkTheme>;
    })
    .add("NumberCard", () => {
        // Props
        let titre  = text("title", "Test");
        let icon   = text("icon");
        let valeur = number("value", 5);
        let unit   = text("unit");

        // Rendering
        if (icon === "") { icon = undefined; }
        if (unit === "") { unit = undefined; }

        return <DarkTheme><NumberCard title={ titre } icon={ icon } value={ valeur } unit={ unit } />;</DarkTheme>
    })
    .add("RankingCard", () => {
        // Props
        let titre = text("title", "Test");
        let data  = array("items", ["Test1", "Test2", "Test3"]);

        // Rendering
        return <DarkTheme><RankingCard title={ titre } items={ data } /></DarkTheme>;
    });

storiesOf("DarkTheme|Cards.Charts", module)
    .addDecorator(story => <div style={{ padding: 16 }}>{ story() }</div>)
    .addDecorator(withKnobs)
    .add("BarChart",  () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = array("values", ["uv"]);
        let colors = object("colors");
        let data   = object("data", linedata);

        // Rendering
        return <DarkTheme><BarChartCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } /></DarkTheme>;
    })
    .add("LineChart", () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = array("values", ["uv"]);
        let colors = object("colors");
        let data   = object("data", linedata);

        // Rendering
        return <DarkTheme><LineChartCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } /></DarkTheme>;
    })
    .add("PieChart",  () => {
        // Props
        let title  = text("title", "Test");
        let labels = text("labels", "name");
        let values = text("values", "value");
        let colors = object("colors");
        let data   = object("data", piedata);

        // Rendering
        return <DarkTheme><PieCard title={ title } data={ data } labels={ labels } values={ values } colors={ colors } /></DarkTheme>;
    });