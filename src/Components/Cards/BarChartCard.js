// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Card, CardContent, CardHeader, Divider, Typography } from "@material-ui/core";
import { withTheme } from '@material-ui/core/styles';

import { CartesianGrid, Legend, Bar, BarChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";

// Component
class BarChartCard extends Component {
    render() {
        // Props
        const { title, data, labels, values, colors, theme, style, width, height } = this.props;

        // Rendering
        const bars = values.map((v, i) => <Bar key={`data-${i}`} type="monotone" dataKey={ v }
                                               fill={ (colors ? colors[v] : null) || theme.palette.primary.main } />);

        return (
            <Card style={ style }>
                <CardHeader title={ title } />
                <Divider/>
                <CardContent>
                    <Typography component="div">
                        <ResponsiveContainer width={ width } height={ height }>
                            <BarChart data={ data }>
                                <CartesianGrid strokeDasharray="3 3" stroke={ theme.palette.text.secondary } />
                                <XAxis dataKey={ labels } stroke={ theme.palette.text.primary } />
                                <YAxis stroke={ theme.palette.text.primary } />
                                <Tooltip labelStyle={{ "color": "#000000" }} />
                                <Legend />
                                { bars }
                            </BarChart>
                        </ResponsiveContainer>
                    </Typography>
                </CardContent>
            </Card>
        );
    }
}

// Props
BarChartCard.propTypes = {
    title:  PropTypes.string.isRequired,
    data:   PropTypes.array.isRequired,
    labels: PropTypes.string.isRequired,
    values: PropTypes.array.isRequired,
    colors: PropTypes.object,

    width:  PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    style:  PropTypes.object,
};

BarChartCard.defaultProps = {
    width: "100%",
    height: 280
};

// Theme
const ThemedBarChartCard = withTheme()(BarChartCard);
ThemedBarChartCard.propTypes = BarChartCard.propTypes;

export default ThemedBarChartCard;