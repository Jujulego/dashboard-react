// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Grid, Paper, Typography } from "@material-ui/core";

import "./NumberCard.scss";

class NumberCard extends Component {
    render() {
        // Props
        let { title, value, unit, icon, style } = this.props;
        const asIcon = (icon !== undefined);

        // Rendering
        const sign = value < 0 ? <span className="sign">-</span> : "";
        unit = unit ? <span className="unit">{ unit }</span> : "";

        if (icon) {
            return (
                <Paper className="number-card" style={ style } >
                    <Grid container alignItems="center" spacing={16} wrap="nowrap">
                        <Grid item xs="auto">
                            <Typography>
                                <FontAwesomeIcon icon={ icon } size="4x" fixedWidth />
                            </Typography>
                        </Grid>
                        <Grid item xs>
                            <Typography align={ asIcon ? "right" : "center" } color="textSecondary" variant="h6">{ title }</Typography>
                            <Typography align={ asIcon ? "right" : "center" }>
                                { sign }
                                <span className="big-num">{ Math.abs(value).toLocaleString() }</span>
                                { unit }
                            </Typography>
                        </Grid>
                    </Grid>
                </Paper>
            );
        }

        return (
            <Paper className="number-card no-icon" style={ style } >
                <Grid container alignItems="flex-end" spacing={16} wrap="nowrap">
                    <Grid item xs="auto">
                        <Typography align={ asIcon ? "right" : "center" } color="textSecondary" variant="h6">{ title }</Typography>
                    </Grid>
                    <Grid item xs>
                        <Typography align={ asIcon ? "right" : "center" }>
                            { sign }
                            <span className="big-num">{ Math.abs(value).toLocaleString() }</span>
                            { unit }
                        </Typography>
                    </Grid>
                </Grid>
            </Paper>
        );
    }
}

// Props
NumberCard.propTypes = {
    icon:  PropTypes.string,
    title: PropTypes.string.isRequired,
    unit:  PropTypes.string,
    value: PropTypes.number.isRequired,

    style: PropTypes.object
};

export default NumberCard;