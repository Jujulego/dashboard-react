// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import {Card, CardContent, CardHeader, Divider, Typography} from "@material-ui/core";
import { withTheme } from '@material-ui/core/styles';

import { CartesianGrid, Legend, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis } from "recharts";

// Component
class LineChartCard extends Component {
    render() {
        // Props
        const { title, data, labels, values, colors, names, theme, style, width, height } = this.props;

        // Rendering
        const lines = values.map((v, i) => <Line key={`data-${i}`} type="monotone" dataKey={ v } name={ (names ? names[v] : null) }
                                                 stroke={ (colors ? colors[v] : null) || theme.palette.primary.main } />);

        return (
            <Card style={ style }>
                <CardHeader title={ title } />
                <Divider/>
                <CardContent>
                    <Typography component="div">
                        <ResponsiveContainer width={ width } height={ height }>
                            <LineChart data={ data }>
                                <CartesianGrid strokeDasharray="3 3" stroke={ theme.palette.text.secondary } />
                                <XAxis dataKey={ labels } stroke={ theme.palette.text.primary } />
                                <YAxis stroke={ theme.palette.text.primary } />
                                <Tooltip labelStyle={{ "color": "#000000" }} />
                                <Legend />
                                { lines }
                            </LineChart>
                        </ResponsiveContainer>
                    </Typography>
                </CardContent>
            </Card>
        );
    }
}

// Props
LineChartCard.propTypes = {
    title:  PropTypes.string.isRequired, /** Titre */
    data:   PropTypes.array.isRequired,  /** Données */
    labels: PropTypes.string.isRequired,
    values: PropTypes.array.isRequired,
    colors: PropTypes.object,
    names:  PropTypes.object,

    width:  PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    style:  PropTypes.object,
};

LineChartCard.defaultProps = {
    width: "100%",
    height: 280
};

// Theme
const ThemedLineChartCard = withTheme()(LineChartCard);
ThemedLineChartCard.propTypes = LineChartCard.propTypes;

export default ThemedLineChartCard;