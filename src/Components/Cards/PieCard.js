// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Card, CardContent, CardHeader, Divider, Typography } from "@material-ui/core";
import { withTheme } from "@material-ui/core/es/styles";

import { Cell, Pie, PieChart, ResponsiveContainer, Tooltip } from "recharts";

// Component
class PieCard extends Component {
    // Méthodes
    render() {
        // Props
        const { title, data, labels, values, radius, theme, style, width, height } = this.props;

        // Rendering
        const cells = data.map((obj, i) => <Cell key={`cell-${i}`} stroke={ theme.palette.background.paper }
                                                 fill={ this.color(obj, i) } />);

        return (
            <Card style={ style }>
                <CardHeader title={ title } />
                <Divider/>
                <CardContent>
                    <Typography component="div">
                        <ResponsiveContainer width={ width || (radius * 2 + 100) } height={ height || (radius * 2 + 50) }>
                            <PieChart>
                                <Pie data={ data } nameKey={ labels } dataKey={ values }
                                     label={({cx, cy, midAngle, innerRadius, outerRadius, percent, index}) => data[index][labels] || data[index][values] }
                                     cx="50%" cy="50%" outerRadius={ radius }>
                                    { cells }
                                </Pie>
                                <Tooltip />
                            </PieChart>
                        </ResponsiveContainer>
                    </Typography>
                </CardContent>
            </Card>
        );
    }

    color(obj, i) {
        const { colors, labels, theme } = this.props;

        if (colors) {
            if (colors instanceof Array) {
                return colors[i % colors.length][500];
            } else {
                return colors[obj[labels]][500];
            }
        } else {
            return theme.palette.primary.main;
        }
    }
}

// Props
PieCard.propTypes = {
    title:  PropTypes.string.isRequired,
    data:   PropTypes.array.isRequired,
    labels: PropTypes.string.isRequired,
    values: PropTypes.string.isRequired,
    colors: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    radius: PropTypes.number,

    width:  PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    style:  PropTypes.object,
};

PieCard.defaultProps = {
    radius: 50,
    width: "100%"
};

// Theme
const ThemedPieChart = withTheme()(PieCard);
ThemedPieChart.propTypes = PieCard.propTypes;
ThemedPieChart.defaultProps = PieCard.defaultProps;

export default ThemedPieChart;