// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Card, CardHeader, List, ListItem, ListItemIcon, ListItemText, Divider } from "@material-ui/core";

// Component
class RankingCard extends Component {
    render() {
        // Props
        let { title, items, style } = this.props;

        // Rendering
        items = items.map(
            (it, i) => (
                <ListItem key={`item-${i}`}>
                    <ListItemIcon><span>#{ i+1 }</span></ListItemIcon>
                    <ListItemText primary={it} />
                </ListItem>
            )
        );

        return (
            <Card style={ style }>
                <CardHeader title={ title }/>
                <Divider/>
                <List>
                    { items }
                </List>
            </Card>
        );
    }
}

// Props
RankingCard.propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.string.isRequired).isRequired,

    style: PropTypes.object,
};

export default RankingCard;