// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import { Card, CardHeader, Divider, List, ListItem, ListItemText } from "@material-ui/core";

// Component
class ListCard extends Component {
    render() {
        // Props
        let { title, items, style } = this.props;

        // Rendering
        items = items.map(
            (it, i) => (
                <ListItem key={`item-${i}`}>
                    <ListItemText primary={it} />
                </ListItem>
            )
        );

        return (
            <Card style={ style }>
                <CardHeader title={ title }/>
                <Divider />
                <List>
                    { items }
                </List>
            </Card>
        );
    }
}

// Props
ListCard.propTypes = {
    title: PropTypes.string.isRequired,
    items: PropTypes.arrayOf(PropTypes.string),

    style:  PropTypes.object,
};

export default ListCard;