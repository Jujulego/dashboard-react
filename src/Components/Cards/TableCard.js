// Importations
import React, { Component } from "react";
import { Link as RouterLink } from "react-router-dom";

import classNames from "classnames";
import PropTypes from "prop-types";

import { Checkbox, IconButton, Link, Paper, Table, TableBody, TableCell, TableHead, TablePagination, TableRow, TableSortLabel, Toolbar, Typography, withStyles } from "@material-ui/core";
import { lighten } from "@material-ui/core/styles/colorManipulator";
import { Sync as SyncIcon, Delete as DeleteIcon } from "@material-ui/icons";

// Style
const style = theme => ({
    root: {
        overflow: "hidden"
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            } : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    title: {
        flex: "0 0 auto",
    },
    spacer: {
        flex: "1 1 100%",
    },
    checkBoxCol: {
        width: 32
    }
});

// Function
function getLabel(champ) {
    return champ.label || champ.prop;
}

function getValue(champ, obj) {
    if (champ.format) {
        return champ.format(obj);
    }

    let val = obj[champ.prop];
    if (val && typeof val !== "string") {
        val = val.toLocaleString();
    }

    return val;
}

function desc(a, b, orderBy) {
    const va = getValue(orderBy, a);
    const vb = getValue(orderBy, b);

    if (vb < va) {
        return -1;
    }
    if (vb > va) {
        return 1;
    }
    return 0;
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });

    return stabilizedThis.map(el => el[0]);
}

// Sub-components
function HeaderCell({ champ, sorted, order, onSort }) {
    if (champ.sortable) {
        return (
            <TableCell align={champ.main ? "left" : "right"} padding={champ.main ? "none" : "default"}>
                <TableSortLabel active={sorted} direction={order}
                                onClick={() => onSort(champ)}>
                    {getLabel(champ)}
                </TableSortLabel>
            </TableCell>
        );
    } else {
        return (
            <TableCell align={champ.main ? "left" : "right"} padding={champ.main ? "none" : "default"}>
                { getLabel(champ) }
            </TableCell>
        );
    }
}

function DataCell({ champ, obj }) {
    // Prepare value
    let val = getValue(champ, obj);

    // Render
    if (champ.link) {
        val = <Link to={ champ.link(obj) } color="textPrimary" component={ RouterLink }>{ val }</Link>
    }

    if (champ.main === true) {
        return <TableCell component="th" scope="row" padding="none">{ val }</TableCell>;
    } else {
        return <TableCell align="right">{ val }</TableCell>;
    }
}
function DataRow({ champs, obj, selected, onClick }) {
    return (
        <TableRow hover selected={ selected }>
            <TableCell padding="checkbox">
                <Checkbox checked={ selected } onClick={ onClick } />
            </TableCell>
            { champs.map((champ, i) => <DataCell key={`${obj._id}-${i}`} champ={ champ } obj={ obj } />) }
        </TableRow>
    );
}

// Classe
class TableCard extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Init state
        this.state = {
            selected: [],
            sort: props.champs.find((e) => e.main) || null, order: "asc",
            page: 0, pageSize: 10
        }
    }

    // Méthodes
    isSelected(obj) {
        return this.state.selected.indexOf(obj._id) !== -1;
    }

    // - rendering
    render() {
        // Props
        const { title, champs, data, classes, style } = this.props;
        const { selected, sort, order, page, pageSize } = this.state;

        // Rendering
        const nbRows = data.length;
        const nbSelected = selected.length;

        let action = (
            <IconButton onClick={() => this.handleSyncClick()}>
                <SyncIcon />
            </IconButton>
        );

        if (nbSelected !== 0) {
            action = (
                <IconButton onClick={() => this.handleDeleteClick()}>
                    <DeleteIcon />
                </IconButton>
            )
        }

        return (
            <Paper className={classes.root} style={ style }>
                <Toolbar className={classNames({ [classes.highlight]: nbSelected > 0 })}>
                    <div className={classes.title}>{
                        (nbSelected === 0)
                            ? (<Typography id="table-title" variant="h6">{title}</Typography>)
                            : (<Typography variant="subtitle2">{ `${nbSelected} sélectionné(s)` }</Typography>)
                    }</div>
                    <div className={classes.spacer} />
                    <div>{ action }</div>
                </Toolbar>
                <Table aria-labelledby="#table-title">
                    <TableHead>
                        <TableRow>
                            <TableCell className={ classes.checkBoxCol } padding="checkbox">
                                <Checkbox checked={ nbRows !== 0 && nbRows === nbSelected }
                                          indeterminate={ nbSelected > 0 && nbSelected !== nbRows }
                                          onChange={() => this.handleSelectAll()} />
                            </TableCell>
                            { champs.map((champ, i) =>
                                <HeaderCell key={i} champ={champ} order={order}
                                            sorted={sort != null && sort.prop === champ.prop}
                                            onSort={(c) => this.handleSort(c)} />)
                            }
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        { stableSort(data, getSorting(order, sort))
                            .slice(page * pageSize, (page + 1) * pageSize)
                            .map((obj) =>
                                <DataRow key={ obj._id } obj={ obj } onClick={() => this.handleRowSelect(obj) }
                                         champs={ champs } selected={ this.isSelected(obj) } />)
                        }
                    </TableBody>
                </Table>
                <TablePagination component="div" count={data.length} page={page}
                     rowsPerPage={pageSize} rowsPerPageOptions={[10, 25, 50]}
                     onChangePage={(e, np) => this.setState({ page: np })}
                     onChangeRowsPerPage={(e) => this.handleChangePageSize(e.target.value)}/>
            </Paper>
        );
    }

    // - events
    handleSyncClick() {
        const { onSync } = this.props;
        if (onSync) onSync();
    }
    handleDeleteClick() {
        const { onDelete } = this.props;
        if (onDelete) onDelete(this.state.selected);

        this.setState({ selected: [] });
    }

    handleSelectAll() {
        const { selected } = this.state;
        const { data } = this.props;

        let newSelected = selected;

        if (selected.length !== data.length) {
            // Select all
            for (let obj of data) {
                if (!this.isSelected(obj)) {
                    newSelected = newSelected.concat(obj._id);
                }
            }
        } else {
            // Unselect all
            newSelected = [];
        }

        this.setState({ selected: newSelected });
    }
    handleRowSelect(obj) {
        const { selected } = this.state;

        const selectedIndex = selected.indexOf(obj._id);
        let newSelected = [];

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, obj._id);
        } else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1));
        } else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1));
        } else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            );
        }

        this.setState({ selected: newSelected });
    }

    handleSort(champ) {
        const { sort, order } = this.state;

        if (sort.prop === champ.prop) {
            this.setState({ order: (order === "desc" ? "asc" : "desc") });
        } else {
            this.setState({ sort: champ, order: "desc" });
        }
    }
    handleChangePageSize(nps) {
        let { page, pageSize } = this.state;

        // Compute new page
        if (page !== 0) {
            page = Math.floor((page * pageSize) / nps);
        }

        this.setState({ page: page, pageSize: nps });
    }
}

// Props
TableCard.propTypes = {
    title: PropTypes.string.isRequired,
    data: PropTypes.arrayOf(
        PropTypes.shape({
            _id: PropTypes.any.isRequired,
        }).isRequired
    ).isRequired,
    champs: PropTypes.arrayOf(
        PropTypes.shape({
            label:    PropTypes.string, // label du champ
            prop:     PropTypes.string.isRequired, // propriété affichée (sans traitements)
            format:   PropTypes.func,   // Mise en forme de la donnée
            link:     PropTypes.func,   // fonction donnant une url pour le lien associé à la donnée (abs = pas de lien)
            sortable: PropTypes.bool,   // active le tri
            main:     PropTypes.bool,   // champ principal (balise th)
        }).isRequired
    ).isRequired,

    onSync: PropTypes.func,
    onDelete: PropTypes.func,
    style: PropTypes.object
};

export default withStyles(style)(TableCard);