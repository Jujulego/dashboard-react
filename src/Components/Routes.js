// Importations
import React, { Component } from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

import { withTheme } from "@material-ui/core";

import Admin from "./Admin/Admin";
import Home from "./Home/Home";
import NavBarLayout from "./NavBar/NavBarLayout";

// Component
class Routes extends Component {
    render() {
        // Props
        const { theme } = this.props;

        // Rendering
        return (
            <Router>
                <NavBarLayout spacing={ theme.spacing.unit * 2 }>
                    <Route exact path="/" component={ Home } />
                    <Route path="/admin"  component={ Admin } />
                </NavBarLayout>
            </Router>
        );
    }
}

export default withTheme()(Routes);