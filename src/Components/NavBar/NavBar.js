// Importations
import React, { Component } from 'react';
import { Link as RouterLink } from "react-router-dom";

import { AppBar, Button, Link, Toolbar, Typography } from '@material-ui/core';

// Component
class NavBar extends Component {
    // Méthodes
    render() {
        return (
            <AppBar position="static">
                <Toolbar>
                    <Typography variant="h6" color="inherit" style={{ flex: 1 }}>
                        <Link color="inherit" to="/" component={ RouterLink }>Dashboard</Link>
                    </Typography>
                    <Button to="/admin" component={ RouterLink }>Artistes</Button>
                    <Button to="/admin/album" component={ RouterLink }>Albums</Button>
                    <Button to="/admin/track" component={ RouterLink }>Tracks</Button>
                </Toolbar>
            </AppBar>
        );
    }
}

export default NavBar;