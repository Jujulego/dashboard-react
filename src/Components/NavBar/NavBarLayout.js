// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";

import "./NavBarLayout.scss";
import NavBar from "./NavBar";
import DarkTheme from "../DarkTheme";

// Component
class NavBarLayout extends Component {
    // Méthodes
    render() {
        // Props
        const spacing = this.props.spacing;

        // Rendering
        return (
            <DarkTheme>
                <div className="layout">
                    <NavBar />
                    <div className="content" style={{ padding: spacing }}>
                        { this.props.children }
                    </div>
                </div>
            </DarkTheme>
        );
    }
}

// Props
NavBarLayout.propTypes = {
    spacing: PropTypes.number,
};

NavBarLayout.defaultProps = {
    spacing: 0
};

export default NavBarLayout;