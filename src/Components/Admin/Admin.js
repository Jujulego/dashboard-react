// Importations
import React, { Component } from "react";
import { Route } from "react-router-dom";

import Albums from "./Albums/Albums";
import Artists from "./Artists/Artists";
import Tracks from "./Tracks/Tracks";

import Album from "./Albums/Album";
import Artist from "./Artists/Artist";
import Track from "./Tracks/Track";

// Component
class Admin extends Component {
    render() {
        // Props
        const { match } = this.props;

        // Rendering
        return (
            <>
                <Route exact path={`${match.path}/`} component={ Artists } />
                <Route path={`${match.path}/:id([0-9a-f]+)/`} component={ Artist } />

                <Route exact path={`${match.path}/album`} component={ Albums } />
                <Route path={`${match.path}/album/:id([0-9a-f]+)/`} component={ Album } />

                <Route exact path={`${match.path}/track`} component={ Tracks } />
                <Route path={`${match.path}/track/:id([0-9a-f]+)/`} component={ Track } />
            </>
        );
    }
}

export default Admin;