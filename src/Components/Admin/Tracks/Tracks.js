// Importations
import React, { Component } from 'react';
import PropTypes from "prop-types";
import axios from "axios";

import { Grid, withTheme } from "@material-ui/core";

import TableCard from "../../Cards/TableCard";
import NewTrack from "./NewTrack";

// Component
class Tracks extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            tracks: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { theme, album } = this.props;
        const { tracks } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;
        const champs = [
            {
                label: "Titre",
                prop: "title",
                main: true,
                sortable: true,
                link: (obj) => `/admin/track/${ obj._id }`
            },
            {
                label: "Durée",
                prop: "duration",
                sortable: true,
                format: (obj) => `${Math.floor(obj.duration / 60)} min ${obj.duration % 60} sec`
            },
            {
                label: "Ecoutes",
                prop: "listenings",
                sortable: true
            },
            {
                label: "Likes",
                prop: "likes",
                sortable: true
            },
        ];

        return (
            <Grid container spacing={spacing}>
                <Grid item xs={12} md>
                    <TableCard title="Morceaux" data={ tracks } champs={ champs }
                               onSync={() => this.sync()}
                               onDelete={(ids) => this.handleDelete(ids)} />
                </Grid>
                { (album) ? (
                    <Grid item xs={12} md={4}>
                        <NewTrack onAdd={(t) => this.handleAdd(t)} />
                    </Grid>
                ) : null }
            </Grid>
        );
    }

    sync() {
        // Props
        const { album } = this.props;

        // Requetes
        if (album) {
            axios.get(`http://localhost:8000/api/album/${album._id}/tracks`)
                .then((res) => this.setState({ tracks: res.data }));
        } else {
            axios.get(`http://localhost:8000/api/track/all`)
                .then((res) => this.setState({ tracks: res.data }));
        }
    }

    handleAdd(track) {
        const { album } = this.props;
        const { tracks } = this.state;

        this.setState({ tracks: [ ...tracks, track ] });

        if (album) {
            // Add to album
            axios.put(`http://localhost:8000/api/album/${album._id}/tracks`, { track: track._id });
        }
    }
    handleDelete(ids) {
        const { album  } = this.props;
        const { tracks } = this.state;

        // Update UI
        this.setState({ tracks: tracks.filter((t) => ids.indexOf(t._id) === -1 ) });

        // Delete !
        for (const id of ids) {
            if (album) {
                axios.delete(`http://localhost:8000/api/album/${album._id}/tracks/${id}`);
            }

            axios.delete(`http://localhost:8000/api/track/${id}`);
        }
    }
}

// Props
Tracks.propTypes = {
    album: PropTypes.object.isRequired
};

export default withTheme()(Tracks);