// Importations
import React, { Component } from "react";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, Typography, withTheme } from "@material-ui/core";
import Artists from "../Artists/Artists";

// Component
class Track extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            track: null
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { theme } = this.props;
        const { id } = this.props.match.params;

        const { track } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;

        if (!track) {
            return <Typography>{ id }</Typography>
        }

        return (
            <Grid container direction="column" spacing={ spacing }>
                <Grid item>
                    <Card>
                        <CardHeader title={ `Morceau ${track.title}` } titleTypographyProps={{ variant: "h6" }} />
                        <CardContent>
                            <Grid container spacing={ spacing }>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Titre" value={ track.title } fullWidth required
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("title") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Durée (en secondes)" value={ track.duration } type="number" fullWidth required
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("duration") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Ecoutes" value={ track.listenings } type="number" fullWidth
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("listenings") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Likes" value={ track.likes } type="number" fullWidth
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("likes") } />
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions style={{ paddingLeft: spacing/2, paddingRight: spacing/2 }}>
                            <Grid container spacing={ spacing } justify="flex-end">
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="secondary" fullWidth
                                            onClick={() => this.sync()}>Annuler</Button>
                                </Grid>
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="primary" fullWidth
                                            onClick={() => this.handleSave()}>Modifier</Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Artists track={ track } title={ "Chanteurs" } />
                </Grid>
            </Grid>
        );
    }

    sync() {
        // Props
        const { id } = this.props.match.params;

        // Requete
        axios.get(`http://localhost:8000/api/track/${id}`)
            .then((res) => this.setState({ track: res.data }) );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            const { track } = this.state;
            track[prop] = val;

            this.setState({ track })
        }
    }

    handleSave() {
        const { track } = this.state;

        if (track === null) {
            return;
        }

        axios.post(`http://localhost:8000/api/track/${track._id}`, track)
            .then((res) => this.setState({ track: res.data }) );
    }
}

export default withTheme()(Track);