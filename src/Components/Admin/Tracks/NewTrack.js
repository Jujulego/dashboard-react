// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, withTheme } from "@material-ui/core";

// Component
class NewTrack extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            title:      '',
            duration:   '',
            listenings: '0',
            likes:      '0'
        }
    }

    // Méthodes
    render() {
        // Props
        const { theme } = this.props;
        const { title, duration, listenings, likes } = this.state;

        // Rendering
        const spacing = theme.spacing.unit;
        const valid = (title !== "") && (duration !== "");

        return (
            <form autoComplete="off" noValidate>
                <Card>
                    <CardHeader title="Nouveau morceau" titleTypographyProps={{ variant: "h6" }} />
                    <CardContent>
                        <Grid container direction="column" spacing={ spacing }>
                            <Grid item xs>
                                <TextField label="Titre" value={ title } fullWidth required
                                           onChange={ this.handleChange("title") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Durée (en secondes)" value={ duration } type="number" fullWidth required
                                           onChange={ this.handleChange("duration") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Ecoutes" value={ listenings } type="number" fullWidth
                                           InputLabelProps={{ shrink: true }}
                                           onChange={ this.handleChange("listenings") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Likes" value={ likes } type="number" fullWidth
                                           InputLabelProps={{ shrink: true }}
                                           onChange={ this.handleChange("likes") } />
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" color="primary" fullWidth
                                disabled={ !valid } onClick={ this.handleAdd }>Ajouter</Button>
                    </CardActions>
                </Card>
            </form>
        );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            this.setState({
                [prop]: val
            })
        }
    }

    handleAdd = () => {
        // Send !
        axios.put("http://localhost:8000/api/track/", this.state)
            .then((rep) => {
                // Callback
                const { onAdd } = this.props;

                if (onAdd) {
                    onAdd(rep.data);
                }
            })
            .catch((err) => {
                console.error(err);
            })
            .then(() => {
                // Reset !
                this.setState({
                    title:      '',
                    duration:   '',
                    listenings: '0',
                    likes:      '0'
                });
            })
    };
}

// Props
NewTrack.propTypes = {
    onAdd: PropTypes.func
};

export default withTheme()(NewTrack);