// Importations
import React, { Component } from 'react';
import PropTypes from "prop-types";
import axios from "axios";

import { Grid, withTheme } from "@material-ui/core";

import TableCard from "../../Cards/TableCard";
import NewAlbum from "./NewAlbum";

// Component
class Albums extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            albums: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { theme, artist } = this.props;
        const { albums } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;
        const champs = [
            {
                label: "Titre",
                prop: "title",
                main: true,
                sortable: true,
                link: (obj) => `/admin/album/${ obj._id }`
            },
            {
                label: "Genre",
                prop: "genre",
                sortable: true
            },
            {
                label: "Date de sortie",
                prop: "release",
                sortable: true,
                format: (obj) => new Date(obj.release).toLocaleDateString()
            },
            {
                label: "Morceaux",
                prop: "tracks",
                sortable: true,
                format: (obj) => obj.tracks.length.toLocaleString()
            }
        ];

        return (
            <Grid container spacing={spacing}>
                <Grid item xs={12} md>
                    <TableCard title="Albums" data={ albums } champs={ champs }
                               onSync={() => this.sync()}
                               onDelete={(ids) => this.handleDelete(ids)} />
                </Grid>
                { (artist) ? (
                    <Grid item xs={12} md={4}>
                        <NewAlbum onAdd={(a) => this.handleAdd(a)} />
                    </Grid>
                ) : null }
            </Grid>
        );
    }

    sync() {
        // Props
        const { artist } = this.props;

        // Requetes
        if (artist) {
            axios.get(`http://localhost:8000/api/artist/${artist._id}/albums`)
                .then((res) => this.setState({ albums: res.data }));
        } else {
            axios.get(`http://localhost:8000/api/album/all`)
                .then((res) => this.setState({ albums: res.data }));
        }
    }

    handleAdd(album) {
        const { artist } = this.props;
        const { albums } = this.state;

        this.setState({ albums: [ ...albums, album ] });

        if (artist) {
            // Add to artist
            axios.put(`http://localhost:8000/api/artist/${artist._id}/albums`, { album: album._id });
        }
    }
    handleDelete(ids) {
        const { artist } = this.props;
        const { albums } = this.state;

        // Update UI
        this.setState({ albums: albums.filter((a) => ids.indexOf(a._id) === -1 ) });

        // Delete !
        for (const id of ids) {
            if (artist) {
                axios.delete(`http://localhost:8000/api/artist/${artist._id}/albums/${id}`);
            }

            axios.delete(`http://localhost:8000/api/album/${id}`);
        }
    }
}

// Props
Albums.propTypes = {
    artist: PropTypes.object
};

export default withTheme()(Albums);