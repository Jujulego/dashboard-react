// Importations
import React, { Component } from "react";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, Typography, withTheme, withStyles } from "@material-ui/core";
import Tracks from "../Tracks/Tracks";

// Style
const style = theme => ({
    cover: {
        width: 150,
        marginRight: theme.spacing.unit * 2
    }
});

// Component
class Album extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            album: null
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { theme, classes } = this.props;
        const { id } = this.props.match.params;

        const { album } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;

        if (!album) {
            return <Typography>{ id }</Typography>
        }

        const release = new Date(album.release).toISOString().split(/[T ]/)[0];

        return (
            <Grid container direction="column" spacing={ spacing }>
                <Grid item>
                    <Card>
                        <CardHeader title={ `Album ${album.title}` } titleTypographyProps={{ variant: "h6" }} />
                        <CardContent>
                            <Grid container spacing={ spacing }>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Titre" value={ album.title } fullWidth required
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("title") } />

                                    <TextField label="Genre" value={ album.genre } fullWidth required
                                               style={{ marginBottom: spacing }}
                                               onChange={ this.handleChange("genre") } />

                                    <TextField label="Date de sortie" type="date" value={ release } fullWidth required
                                               InputLabelProps={{ shrink: true }}
                                               onChange={ this.handleChange("release") } />
                                </Grid>
                                <Grid item xs={12} md={6} container alignItems="center">
                                    <Grid item xs="auto">
                                        <img className={ classes.cover } alt="Couverture de l'album" src={ album.cover } />
                                    </Grid>

                                    <Grid item xs>
                                        <TextField label="Cover" type="url" value={ album.cover } fullWidth
                                                   onChange={ this.handleChange("cover") } />
                                    </Grid>
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions style={{ paddingLeft: spacing/2, paddingRight: spacing/2 }}>
                            <Grid container spacing={ spacing } justify="flex-end">
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="secondary" fullWidth
                                            onClick={() => this.sync()}>Annuler</Button>
                                </Grid>
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="primary" fullWidth
                                            onClick={() => this.handleSave()}>Modifier</Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Tracks album={ album }/>
                </Grid>
            </Grid>
        );
    }

    sync() {
        // Props
        const { id } = this.props.match.params;

        // Requete
        axios.get(`http://localhost:8000/api/album/${id}`)
            .then((res) => this.setState({ album: res.data }) );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            const { album } = this.state;
            album[prop] = val;

            this.setState({ album })
        }
    }

    handleSave() {
        const { album } = this.state;

        if (album === null) {
            return;
        }

        axios.post(`http://localhost:8000/api/album/${album._id}`, album)
            .then((res) => this.setState({ album: res.data }) );
    }
}

export default withTheme()(withStyles(style)(Album));