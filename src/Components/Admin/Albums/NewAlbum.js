// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, withTheme } from "@material-ui/core";

// Component
class NewAlbum extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            title: '',
            genre: '',
            release: ''
        }
    }

    // Méthodes
    render() {
        // Props
        const { theme } = this.props;
        const { title, genre, release } = this.state;

        // Rendering
        const spacing = theme.spacing.unit;
        const valid = (title !== "") && (genre !== "") && (release !== "");

        return (
            <form autoComplete="off" noValidate>
                <Card>
                    <CardHeader title="Nouvel album" titleTypographyProps={{ variant: "h6" }} />
                    <CardContent>
                        <Grid container direction="column" spacing={ spacing }>
                            <Grid item xs>
                                <TextField label="Titre" value={ title } fullWidth required
                                           onChange={ this.handleChange("title") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Genre" value={ genre } fullWidth required
                                           onChange={ this.handleChange("genre") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Date de sortie" value={ release } type="date" fullWidth required
                                           InputLabelProps={{ shrink: true }}
                                           onChange={ this.handleChange("release") } />
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" color="primary" fullWidth
                                disabled={ !valid } onClick={ this.handleAdd }>Ajouter</Button>
                    </CardActions>
                </Card>
            </form>
        );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            this.setState({
                [prop]: val
            })
        }
    }

    handleAdd = () => {
        // Send !
        axios.put("http://localhost:8000/api/album/", this.state)
            .then((rep) => {
                // Callback
                const { onAdd } = this.props;

                if (onAdd) {
                    onAdd(rep.data);
                }
            })
            .catch((err) => {
                console.error(err);
            })
            .then(() => {
                // Reset !
                this.setState({
                    title: '',
                    genre: ''
                });
            })
    };
}

// Props
NewAlbum.propTypes = {
    onAdd: PropTypes.func
};

export default withTheme()(NewAlbum);