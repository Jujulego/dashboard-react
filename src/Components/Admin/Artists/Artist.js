// Importations
import React, { Component } from "react";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, Typography, withTheme } from "@material-ui/core";

import Albums from "../Albums/Albums";

// Component
class Artist extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            artist: null
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { theme } = this.props;
        const { id } = this.props.match.params;

        const { artist } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;

        if (!artist) {
            return <Typography>{ id }</Typography>
        }

        const birth = new Date(artist.birth).toISOString().split(/[T ]/)[0];

        return (
            <Grid container direction="column" spacing={ spacing }>
                <Grid item>
                    <Card>
                        <CardHeader title={ `Artiste ${artist.first_name} ${artist.name}` } titleTypographyProps={{ variant: "h6" }} />
                        <CardContent>
                            <Grid container spacing={ spacing }>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Nom" value={ artist.name } fullWidth required
                                               onChange={ this.handleChange("name") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Prénom" value={ artist.first_name } fullWidth
                                               onChange={ this.handleChange("first_name") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Date de naissance" type="date" value={ birth } fullWidth required
                                               InputLabelProps={{ shrink: true }}
                                               onChange={ this.handleChange("birth") } />
                                </Grid>
                                <Grid item xs={12} md={6}>
                                    <TextField label="Followers" type="number" value={ artist.followers } fullWidth
                                               onChange={ this.handleChange("followers") } />
                                </Grid>
                            </Grid>
                        </CardContent>
                        <CardActions style={{ paddingLeft: spacing/2, paddingRight: spacing/2 }}>
                            <Grid container spacing={ spacing } justify="flex-end">
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="secondary" fullWidth
                                            onClick={() => this.sync()}>Annuler</Button>
                                </Grid>
                                <Grid item xs md={2}>
                                    <Button variant="contained" color="primary" fullWidth
                                            onClick={() => this.handleSave()}>Modifier</Button>
                                </Grid>
                            </Grid>
                        </CardActions>
                    </Card>
                </Grid>
                <Grid item>
                    <Albums artist={ artist } />
                </Grid>
            </Grid>
        );
    }

    sync() {
        // Props
        const { id } = this.props.match.params;

        // Requete
        axios.get(`http://localhost:8000/api/artist/${id}`)
            .then((res) => this.setState({ artist: res.data }) );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            const { artist } = this.state;
            artist[prop] = val;

            this.setState({ artist })
        }
    }

    handleSave() {
        const { artist } = this.state;

        if (artist === null) {
            return;
        }

        axios.post("http://localhost:8000/api/artist/" + artist._id, artist)
            .then((res) => this.setState({ artist: res.data }) );
    }
}

export default withTheme()(Artist);