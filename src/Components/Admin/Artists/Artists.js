// Importations
import React, { Component } from "react";
import axios from "axios";
import PropTypes from "prop-types";

import { Grid, withTheme } from "@material-ui/core";

import NewArtist from "./NewArtist";
import SearchArtist from "./SearchArtist";
import TableCard from "../../Cards/TableCard";

// Component
class Artists extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            artists: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    // - rendering
    render() {
        // Props
        const { theme, title, track } = this.props;
        const { artists } = this.state;

        // Rendering
        const spacing = theme.spacing.unit * 2;
        const champs = [
            {
                label: "Nom",
                prop: "first_name",
                main: true,
                sortable: true,
                link: (obj) => `/admin/${ obj._id }`,
                format: (obj) => `${obj.first_name} ${obj.name}`
            },
            {
                label: "Date de naissance",
                prop: "birth",
                sortable: true,
                format: (obj) => new Date(obj.birth).toLocaleDateString()
            },
            {
                label: "Followers",
                prop: "followers",
                sortable: true
            },
            {
                label: "Albums",
                prop: "albums",
                sortable: true,
                format: (obj) => obj.albums.length.toLocaleString()
            }
        ];

        return (
            <Grid container spacing={ spacing }>
                <Grid item xs={12} md>
                    <TableCard title={ title || "Artistes" } data={ artists } champs={ champs }
                               onSync={() => this.sync()}
                               onDelete={(ids) => this.handleDelete(ids)} />
                </Grid>
                <Grid item xs={12} md={4}>
                    { (track) ? (
                        <SearchArtist onAdd={ (ids) => this.handleAdds(ids) } />
                    ) : (
                        <NewArtist onAdd={(a) => this.handleAdd(a)} />
                    )}
                </Grid>
            </Grid>
        );
    }

    sync() {
        const { track } = this.props;

        // Requetes
        if (track) {
            axios.get(`http://localhost:8000/api/track/${track._id}/featuring`)
                .then((res) => this.setState({ artists: res.data }));
        } else {
            axios.get("http://localhost:8000/api/artist/all")
                .then((res) => this.setState({ artists: res.data }));
        }
    }

    handleAdd(artist) {
        const { track } = this.props;
        const { artists } = this.state;

        this.setState({ artists: [...artists, artist] });

        // Ajout au morceau
        if (track) {
            axios.put(`http://localhost:8000/api/track/${track._id}/featuring`, { artist: artist._id });
        }
    }

    handleAdds(ids) {
        const { track } = this.props;

        // Ajout au morceau
        console.log("Yo !");
        if (track) {
            ids.forEach((id, i) => {
                axios.put(`http://localhost:8000/api/track/${track._id}/featuring`, { artist: id })
                    .then(() => {
                        if (i === ids.length-1) {
                            this.sync();
                        }
                    })
            });
        }
    }

    handleDelete(ids) {
        // Update UI
        const { track } = this.props;
        const { artists } = this.state;

        this.setState({ artists: artists.filter((a) => ids.indexOf(a._id) === -1 ) });

        // Delete !
        for (const id of ids) {
            if (track) {
                axios.delete(`http://localhost:8000/api/track/${track._id}/featuring/${id}`);
            } else {
                axios.delete(`http://localhost:8000/api/artist/${id}`);
            }
        }
    }
}

// Props
Artists.propTypes = {
    title: PropTypes.string,
    track: PropTypes.object
};

export default withTheme()(Artists);