// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import SearchCard from "../../Cards/SearchCard";

// Classe
class SearchArtist extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Init state
        this.state = {
            data: [],
        }
    }

    // Méthodes
    render() {
        // Props
        const { data } = this.state;
        const { onAdd, style } = this.props;

        console.log(onAdd);

        // Rendering
        const champs = [
            {
                label: "Nom",
                prop: "first_name",
                main: true,
                sortable: true,
                link: (obj) => `/admin/${ obj._id }`,
                format: (obj) => `${obj.first_name} ${obj.name}`
            },
        ];

        return (
            <SearchCard champs={ champs } data={ data } style={ style }
                        onSearch={ (search) => this.handleSearch(search) }
                        onAdd={ onAdd } />
        )
    }

    handleSearch(search) {
        axios.get("http://localhost:8000/api/artist/search/", { params: { q: search } })
            .then((rep) => this.setState({ data: rep.data }));
    }
}

// Props
SearchCard.propTypes = {
    onAdd: PropTypes.func,
    style: PropTypes.object
};

export default SearchArtist;