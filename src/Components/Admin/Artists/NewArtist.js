// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import { Button, Card, CardActions, CardContent, CardHeader, Grid, TextField, withTheme } from "@material-ui/core";

// Component
class NewArtist extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initiate state
        this.state = {
            name: '',
            birth: '',
            first_name: '',
            followers: ''
        }
    }

    // Méthodes
    render() {
        // Props
        const { theme, style } = this.props;
        const { name, birth, first_name, followers } = this.state;

        // Rendering
        const spacing = theme.spacing.unit;
        const valid = (name !== "");

        return (
            <form autoComplete="off" noValidate style={ style }>
                <Card>
                    <CardHeader title="Nouvel artiste" titleTypographyProps={{ variant: "h6" }} />
                    <CardContent>
                        <Grid container direction="column" spacing={ spacing }>
                            <Grid item xs>
                                <TextField label="Nom" value={ name } fullWidth required
                                           onChange={ this.handleChange("name") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Date de naissance" type="date" value={ birth } fullWidth required
                                           InputLabelProps={{ shrink: true }}
                                           onChange={ this.handleChange("birth") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Prénom" value={ first_name } fullWidth
                                           onChange={ this.handleChange("first_name") } />
                            </Grid>
                            <Grid item xs>
                                <TextField label="Followers" type="number" value={ followers } fullWidth
                                           onChange={ this.handleChange("followers") } />
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions>
                        <Button variant="contained" color="primary" fullWidth
                                disabled={ !valid } onClick={ this.handleAdd }>Ajouter</Button>
                    </CardActions>
                </Card>
            </form>
        );
    }

    handleChange(prop) {
        return (event) => {
            // Get new value
            const val = event.target.value;

            // Set state
            this.setState({
                [prop]: val
            })
        }
    }

    handleAdd = () => {
        // Send !
        axios.put("http://localhost:8000/api/artist/", this.state)
            .then((rep) => {
                // Callback
                const { onAdd } = this.props;

                if (onAdd) {
                    onAdd(rep.data);
                }
            })
            .catch((err) => {
                console.error(err);
            })
            .then(() => {
                // Reset !
                this.setState({
                    name: '',
                    first_name: '',
                    followers: ''
                });
            })
    };
}

// Props
NewArtist.propTypes = {
    onAdd: PropTypes.func,
    style: PropTypes.object
};

export default withTheme()(NewArtist);