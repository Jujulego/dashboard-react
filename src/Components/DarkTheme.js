// Importations
import React, { Component } from "react";

import { CssBaseline } from "@material-ui/core";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

// Theme
const theme = createMuiTheme({
    palette: {
        type: "dark",
    },
    typography: { useNextVariants: true },
});

// Component
class DarkTheme extends Component {
    // Méthodes
    render() {
        // Rendering
        return (
            <MuiThemeProvider theme={ theme }>
                <CssBaseline>
                    { this.props.children }
                </CssBaseline>
            </MuiThemeProvider>
        );
    }
}

export default DarkTheme;