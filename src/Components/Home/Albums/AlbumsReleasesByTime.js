// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import LineChartCard from "../../Cards/LineChartCard";
import { blue } from "@material-ui/core/colors";

// Constantes
const MONTHS = [
    "Janvier", "Février", "Mars", "Avril", "Mai", "Juin",
    "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"
];

function formatData(data) {
    return {
        _id: `${MONTHS[data._id.month - 1]} ${data._id.year}`,
        nb:  data.nb
    }
}

// Component
class AlbumsReleasesByTime extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        this.state = {
            data: []
        };
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style, height } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <LineChartCard title="Sorties d'albums" data={ data.map(formatData) } style={ style } height={ height }
                           labels="_id" names={{ "nb": "sorties" }} values={["nb"]} colors={{ "nb": blue[500] }} />
        );
    }

    sync() {
        axios.get("http://localhost:8000/api/album/stats/releases")
            .then((rep) => this.setState({ data: rep.data }));
    }
}

// Props
AlbumsReleasesByTime.propTypes = {
    style: PropTypes.object,
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
};

export default AlbumsReleasesByTime;