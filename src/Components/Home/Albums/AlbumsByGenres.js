// Importations
import React, { Component } from "react";
import axios from "axios";

import { amber, blue, brown, green, purple, red } from "@material-ui/core/colors";

import PieCard from "../../Cards/PieCard";

// Classe
class AlbumsByGenres extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initialiation du state
        this.state = {
            data: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <PieCard title="Albums par genre" data={ data } labels="_id" values="nb" radius={100}
                     colors={[ amber, blue, brown, green, purple, red ]} style={ style } />
        );
    }

    sync() {
        axios.get("http://localhost:8000/api/album/stats/genres")
            .then((rep) => this.setState({ data: rep.data }));
    }
}

export default AlbumsByGenres;