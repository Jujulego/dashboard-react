// Importations
import React, { Component } from "react";
import axios from "axios";

import NumberCard from "../../Cards/NumberCard";

// Classe
class AlbumsCount extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initialiation du state
        this.state = {
            data: 0
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <NumberCard title="Albums" value={ data } style={ style } />
        );
    }

    sync() {
        axios.get("http://localhost:8000/api/album/stats/count")
            .then((rep) => this.setState({ data: rep.data }));
    }
}

export default AlbumsCount;