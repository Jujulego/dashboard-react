// Importations
import React, { Component } from "react";

import { Grid, withTheme } from "@material-ui/core";

import ArtistsStats from "./Artists/ArtistsStats";
import AlbumsStats from "./Albums/AlbumsStats";

// Component
class Home extends Component {
    render() {
        // Props
        const { theme } = this.props;

        // Rendering
        const spacing = theme.spacing.unit;

        return (
            <Grid container spacing={ spacing * 2 }>
                <Grid item xs={12}>
                    <ArtistsStats />
                </Grid>

                <Grid item xs={12}>
                    <AlbumsStats />
                </Grid>
            </Grid>
        )
    }
}

export default withTheme()(Home);