// Importations
import React, { Component } from "react";

import { Grid, withTheme } from "@material-ui/core";

import ArtistsCount from "./ArtistsCount";
import ArtistsRanking from "./ArtistsRanking";
import RankedArtistsStats from "./RankedArtistsStats";

// Component
class ArtistsStats extends Component {
    render() {
        // Props
        const { theme } = this.props;

        // Rendering
        const spacing = theme.spacing.unit;

        return (
            <Grid container spacing={ spacing }>
                <Grid item xs={12} sm={5} md={4} lg={3}>
                    <ArtistsRanking style={{ marginBottom: theme.spacing.unit }} />
                    <ArtistsCount />
                </Grid>

                <Grid item xs={12} sm={7} md={8} lg={9}>
                    <RankedArtistsStats height={330} />
                </Grid>
            </Grid>
        )
    }
}

export default withTheme()(ArtistsStats);