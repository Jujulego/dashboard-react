// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import BarChartCard from "../../Cards/BarChartCard";
import { blue, purple, red } from "@material-ui/core/colors";

// Classe
class RankedArtistsStats extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initialiation du state
        this.state = {
            data: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style, height, width } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <BarChartCard title="Stats artistes populaires" data={ data } style={ style } height={ height } width={ width }
                          labels="full_name" values={["listenings", "likes", "followers"]}
                          colors={{ listenings: blue[500], likes: purple[500], followers: red[500] }} />
        );
    }

    sync() {
        axios.get("http://localhost:8000/api/artist/stats/stars/stats")
            .then((rep) => this.setState({ data: rep.data }));
    }
}

// Props
RankedArtistsStats.propTypes = {
    height: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    width: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    style: PropTypes.object,
};

export default RankedArtistsStats;