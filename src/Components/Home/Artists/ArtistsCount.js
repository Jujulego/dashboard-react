// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import NumberCard from "../../Cards/NumberCard";

// Classe
class ArtistsCount extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initialiation du state
        this.state = {
            data: 0
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <NumberCard title="Artistes" value={ data } style={ style } />
        );
    }

    sync() {
        axios.get("http://localhost:8000/api/artist/stats/count")
            .then((rep) => this.setState({ data: rep.data }));
    }
}

// Props
ArtistsCount.propTypes = {
    style: PropTypes.object,
};

export default ArtistsCount;