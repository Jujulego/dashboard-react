// Importations
import React, { Component } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import RankingCard from "../../Cards/RankingCard";

// Classe
class ArtistsRanking extends Component {
    // Constructeur
    constructor(props) {
        super(props);

        // Initialiation du state
        this.state = {
            data: []
        }
    }

    // Méthodes
    componentDidMount() {
        this.sync();
    }

    render() {
        // Props
        const { style } = this.props;
        const { data } = this.state;

        // Rendering
        return (
            <RankingCard title="Artistes populaires" items={ data.map(d => `${d.first_name} ${d.name}`) } style={ style } />
        );
    }

    sync() {
        const { nb } = this.props;

        axios.get("http://localhost:8000/api/artist/stats/stars", { params: { nb }})
            .then((rep) => this.setState({ data: rep.data }));
    }
}

// Props
ArtistsRanking.propTypes = {
    style: PropTypes.object,
    nb: PropTypes.number,
};

ArtistsRanking.defaultProps = {
    nb: 5
};

export default ArtistsRanking;