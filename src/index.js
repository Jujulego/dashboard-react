import React from 'react';
import ReactDOM from 'react-dom';
import App from './Components/Routes';

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'

import './index.scss';

// FontAwesome
library.add(fas);

// Render !
ReactDOM.render(<App />, document.getElementById('root'));